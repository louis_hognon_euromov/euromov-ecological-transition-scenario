# Euromov ecological transition scenario




Le dossier Scenario_transition contient les données relatives au vote concernant les scénarios de transition écologique du laboratoire Euromov DHM et de son analyse. 

Il contient précisément :
- le pdf décrivant la procédure de vote sur limesurvey 
- les données brutes issues de lime survey sur un fichier Excel (results-survey)
- le script R pour analyser les données et sa version html

Suite à ce vote, l'analyse a fait ressortir que le Scénario n°2 a été élu. 

# VOTE 

La **méthodologie de vote** qui a été utilisée est la suivante : [Système électora à préférences multiples ordonnées ](https://fr.wikipedia.org/wiki/Syst%C3%A8me_%C3%A9lectoral_%C3%A0_pr%C3%A9f%C3%A9rences_multiples_ordonn%C3%A9es)



# CONTACT 

Pour toute question, merci de contacter euromov.shift@gmail.com
